"""
    Fetches Data either using Data Connector or Request Manager
"""

import logging
import os
import sys
import time

import click

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    CLICommandFailedException, MissingFieldException
from xpresso.ai.core.commons.utils.constants import KEY_RUN_NAME, \
    PARAMETERS_COMMIT_ID_KEY, PARAMETERS_FILENAME_KEY
from xpresso.ai.core.commons.utils.generic_utils import move_directory, \
    move_file
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__all__ = ["DataVersioning"]
__author__ = ["Ashritha Goramane", "Gopi Krishna Kanugula"]

logger = XprLogger("data_versioning", level=logging.INFO)


class DataVersioning(AbstractPipelineComponent):
    """
        To push and pull data from pachyderm cluster
    """
    COMPONENT_NAME = "component_name"
    COMMAND_KEY_WORD = "command"
    DATASET_NAME = 'dataset_name'
    DATASET = "dataset"
    REPO_NAME = "repo_name"
    BRANCH_NAME = "branch_name"
    COMMIT_ID = "commit_id"
    DESCRIPTION = "description"
    PUSH_IN_PATH = "push_input_path"
    PULL_IN_PATH = "pull_input_path"
    PULL_OUT_PATH = "pull_output_path"
    XPRESSO_COMMIT_ID = "xpresso_commit_id"
    BRANCH_TYPE = "branch_type"
    DV_COMMIT_ID = "dv_commit_id"

    def __init__(self, xpresso_run_name, parameters_filename,
                 parameters_commit_id, **kwargs):
        super().__init__(name="DataVersioning", run_name=xpresso_run_name,
                         params_filename=parameters_filename,
                         params_commit_id=parameters_commit_id)
        # if you have specified parameters_filename or parameters_commit_id,
        # the run parameters can be accessed from self.run_parameters
        self.cli_args = {}
        self.arguments = kwargs
        self.fetch_arguments()
        self.validate_arguments()
        self.supported_commands = {}
        self.initialize_commands()
        self.version_controller = None
        self.xpr_con = None
        self.start_timestamp = None
        self.end_timestamp = None

    def start(self, xpresso_run_name):
        """
        This is the start method, which does the actual data
        preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the
          Controller that
              the component has started processing (details such as
              the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            xpresso_run_name: xpresso run name which is used by base class to
                identify the current run. It must be passed. While
                running as
                pipeline,
               Xpresso automatically adds it.
        """
        try:
            super().start(xpresso_run_name=xpresso_run_name)
            print("Data versioning component starting", flush=True)
            self.execute()
            print("Data versioning component completed", flush=True)
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the
        end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases
        """
        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def send_metrics(self, status):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": status},
                "metric": {
                    "elapsed_time": self.end_timestamp - self.start_timestamp}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def execute(self):
        """
        Validates the command provided and calls the relevant function for
        execution
        """
        command = self.cli_args[self.COMMAND_KEY_WORD]
        if not command:
            raise CLICommandFailedException("No valid command provided")

        if command not in self.supported_commands:
            raise CLICommandFailedException("Invalid command found")
        try:
            self.fetch_arguments()
            result = self.supported_commands[command]()
            return result
        except TypeError as exception:
            raise CLICommandFailedException(
                "Command cannot be executed {}".format(exception))

    def extract_argument(self, argument):
        """
        Args:
            argument(str): Name of argument to extract
        Returns:
            argument value or None
        """
        if argument in self.arguments:
            return self.arguments[argument]
        return None

    def initialize_commands(self):
        """
        Initialize the supported command variable
        """
        self.supported_commands = {
            "pull_dataset": self.pull_dataset,
            "push_dataset": self.push_dataset
        }

    def validate_push_dataset_input(self):
        """Validates push dataset input arguments """
        if not self.cli_args[self.PUSH_IN_PATH]:
            self.cli_args[self.PUSH_IN_PATH] = "/data"
        mandatory_fields = [
            self.REPO_NAME, self.BRANCH_NAME, self.DATASET_NAME,
            self.PUSH_IN_PATH, self.DESCRIPTION]
        for field in mandatory_fields:
            if not self.cli_args[field]:
                raise MissingFieldException(f"{field} not provided.")

    def push_dataset(self):
        """Push dataset onto pachyderm repo"""
        try:
            self.start_timestamp = time.time()
            self.version_connect()
            self.validate_push_dataset_input()
            push_dataset_input = {
                "repo_name": self.cli_args[self.REPO_NAME],
                "branch_name": self.cli_args[self.BRANCH_NAME],
                "description": self.cli_args[self.DESCRIPTION],
                "dataset_name": self.cli_args[self.DATASET_NAME],
                "path": self.cli_args[self.PUSH_IN_PATH],
                "data_type": "files"
            }
            if self.BRANCH_TYPE in self.cli_args:
                push_dataset_input["type"] = self.cli_args[self.BRANCH_TYPE]
            print("Pushing files onto repo")
            new_commit_id, path_on_cluster = \
                self.version_controller.push_dataset(**push_dataset_input)
            print(f"Push dataset details: {new_commit_id}, {path_on_cluster}")
            self.end_timestamp = time.time()
            self.send_metrics("push_dataset")
            return new_commit_id, path_on_cluster
        except Exception:
            import traceback
            traceback.print_exc()

    def validate_pull_dataset_input(self):
        """
        check if the input provided for pull_dataset is valid or not
        """
        if not self.cli_args[self.PULL_IN_PATH]:
            self.cli_args[self.PULL_IN_PATH] = "/dataset"
        if not self.cli_args[self.PULL_OUT_PATH]:
            self.cli_args[self.PULL_OUT_PATH] = "/data/pull_output"
        mandatory_fields = [
            self.REPO_NAME, self.BRANCH_NAME,
            self.PULL_IN_PATH]
        for field in mandatory_fields:
            if not self.cli_args[field]:
                raise MissingFieldException(f"{field} not provided.")

    def pull_dataset(self):
        """Pulls the data from the pachyderm repo"""
        try:
            self.start_timestamp = time.time()
            self.version_connect()
            self.validate_pull_dataset_input()
            pull_dataset_input = {
                "repo_name": self.cli_args[self.REPO_NAME],
                "branch_name": self.cli_args[self.BRANCH_NAME],
                "path": self.cli_args[self.PULL_IN_PATH],
                "output_type": "files"
            }
            if self.DV_COMMIT_ID in self.cli_args and \
                    self.cli_args[self.DV_COMMIT_ID]:
                pull_dataset_input["commit_id"] = self.cli_args[
                    self.DV_COMMIT_ID]
            elif self.COMMIT_ID in self.cli_args and self.cli_args[
                self.COMMIT_ID]:
                pull_dataset_input["xpresso_commit_id"] = \
                    int(self.cli_args[self.COMMIT_ID])
            elif self.XPRESSO_COMMIT_ID in self.cli_args:
                pull_dataset_input["xpresso_commit_id"] = \
                    int(self.cli_args[self.XPRESSO_COMMIT_ID])

            if self.BRANCH_TYPE in self.cli_args:
                pull_dataset_input["type"] = self.cli_args[self.BRANCH_TYPE]
            new_dir_path = \
                self.version_controller.pull_dataset(**pull_dataset_input)
            rel_data_path = self.cli_args[self.PULL_IN_PATH].strip("/")
            updated_data_path = os.path.join(new_dir_path, rel_data_path)
            if os.path.isdir(updated_data_path):
                move_directory(updated_data_path,
                               self.cli_args[self.PULL_OUT_PATH])
            elif os.path.isfile(updated_data_path):
                move_file(updated_data_path, self.cli_args[self.PULL_OUT_PATH])
            os.system(f'rm -rf {new_dir_path}')
            self.end_timestamp = time.time()
            self.send_metrics("pull_dataset")
        except Exception:
            import traceback
            traceback.print_exc()

    def fetch_arguments(self):
        """
        Fetch arguments form CLI
        Returns:
            Returns arguments
        """
        arguments_key = [self.COMPONENT_NAME, self.COMMAND_KEY_WORD,
                         self.REPO_NAME, self.BRANCH_NAME, self.COMMIT_ID,
                         self.PULL_OUT_PATH, self.PUSH_IN_PATH,
                         self.DESCRIPTION, self.DATASET_NAME, self.DATASET,
                         self.XPRESSO_COMMIT_ID, self.BRANCH_TYPE,
                         self.DV_COMMIT_ID, self.PULL_IN_PATH]
        for arg in arguments_key:
            if arg in self.run_parameters:
                self.cli_args[arg] = self.run_parameters[arg]
                continue
            self.cli_args[arg] = self.extract_argument(arg)

    def version_connect(self):
        """Login to xpresso"""
        try:
            project_name = self.cli_args[self.REPO_NAME]
            self.version_controller = \
                self.controller.fetch_version_controller(
                    project_name, self.xpresso_run_name)
        except Exception as exp:
            raise exp

    def validate_arguments(self):
        if not self.cli_args[self.COMPONENT_NAME]:
            raise CLICommandFailedException("Actual component name mandatory")
        self.name = self.cli_args[self.COMPONENT_NAME]


@click.command()
@click.argument(KEY_RUN_NAME)
@click.argument(PARAMETERS_FILENAME_KEY)
@click.argument(PARAMETERS_COMMIT_ID_KEY)
@click.option('-component-name', type=str,
              help='Actual component name specified during create project.')
@click.option('-command', type=str, help='Versioning command to execute')
@click.option('-repo-name', type=str, help='Name of the repo')
@click.option('-branch-name', type=str, help='Branch name')
@click.option('-description', type=str, help='Description regarding the '
                                             'function to perform')
@click.option('-dataset-name', help="Name of the dataset that will be pushed")
@click.option('-push-input-path', type=str,
              help='Path of the file you want to fetch')
@click.option('-branch-type', type=str,
              help='type of the branch i.e. data or model')
@click.option('-dv-commit-id', type=str, help='Data Versioning Commit Id')
@click.option('-commit-id', type=str, help='Xpresso Commit Id')
@click.option('-xpresso-commit-id', type=int, help='Xpresso commit id')
@click.option('-pull-input-path', type=str, help='Pull dataset input path')
@click.option('-pull-output-path', type=str,
              help='Path where you want to pull')
def cli_options(**kwargs):
    run_name = ""
    params_filename = None
    params_commit_id = None
    if KEY_RUN_NAME in kwargs:
        run_name = kwargs[KEY_RUN_NAME]
        kwargs.pop(KEY_RUN_NAME)
    if PARAMETERS_FILENAME_KEY in kwargs and PARAMETERS_COMMIT_ID_KEY in kwargs:
        params_filename = kwargs[PARAMETERS_FILENAME_KEY] \
            if kwargs[PARAMETERS_FILENAME_KEY] != "None" else None
        kwargs.pop(PARAMETERS_FILENAME_KEY)
        params_commit_id = kwargs[PARAMETERS_COMMIT_ID_KEY] \
            if kwargs[PARAMETERS_COMMIT_ID_KEY] != "None" else None
        kwargs.pop(PARAMETERS_COMMIT_ID_KEY)
    try:
        pipeline_job = DataVersioning(run_name, params_filename,
                                      params_commit_id, **kwargs)
        pipeline_job.start(xpresso_run_name=run_name)
    except Exception as exception:
        click.secho(f"Error:{exception}", err=True, fg="red")


if __name__ == "__main__":
    cli_options()
